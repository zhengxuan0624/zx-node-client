import { createApp } from "vue"
import App from "./App.vue"
import router from "./router"
import "@/styles/common.less"
createApp(App).use(router).mount("#app")

console.log(process.env)
